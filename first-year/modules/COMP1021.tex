\chapter{Mathematics for Computer Science}

Module code: COMP1021

\section{Calculus}

\subsection{Sets and Functions}

A set is an \textbf{unordered} collection of \textbf{distinct} objects or elements.

A tuple is an \textbf{ordered} collection of objects or elements.

\begin{table}[htbp]
	\centering
	\begin{tabular}{@{}lll@{}}
		\toprule
		Symbol                 & Meaning                                 \\
		\midrule
		$A = \{\ldots\}$      & A is a set                              \\
		$B \in A$            & B is a member of A                      \\
		$B \notin A$         & B is not a member of A                  \\
		$A \cup B$           & A union B: Items in either A or B       \\
		$A \cap B$           & A intersection B: Items in both A and B \\
		$A \subseteq B$      & A is a subset of B                      \\
		$A \subset B$        & A is a proper subset of B               \\
		$A \setminus B$      & A minus B                               \\
		$\wp \left(A\right)$ & The powerset of A                       \\
		$A \times B$         & The cartesian product of A and B        \\
		$\varnothing$        & The empty set                           \\
		$\mathbb{N}$         & Natural Numbers                         \\
		$\mathbb{Z}$         & Integers                                \\
		$\mathbb{Q}$         & Rational Numbers                        \\
		$\mathbb{R}$         & Real Numbers                            \\
		$\mathbb{C}$         & Complex Numbers                         \\
		\bottomrule
	\end{tabular}
	\caption{Common symbols in set notation}
\end{table}

\subsubsection{Subset and Proper Subset}

If A is a subset of B ($A \subseteq B$) then every item in A must be an item in B. However, for A to be a \textbf{proper} subset of B ($A \subset B$), then A must be a subset of B \textbf{and} A and B must not be identical.

\textbf{Set builder notation} is used to construct sets around certain rules, e.g.

\[
	S = \{\,n \mid (1 < n < 10) \wedge (n \in \mathbb{Z})\,\}
\]

In english, this means ``$S$ is the set such that each item $n$ is between $1$ and $10$ and $n$ is an integer.''

\subsubsection{Principle of inclusion-exclusion}

\begin{equation}
	|A \cup B| \equiv |A| + |B| - |A \cap B|
\end{equation}

\subsubsection{Rational and Real numbers}

A \textbf{rational} number can be defined as any number in the following set:

\begin{equation}
	\mathbb{Q} \equiv \left\{\frac{m}{n} \mid (m \in \mathbb{Z}) \wedge (n \in \mathbb{Z}) \wedge (n \neq 0)\right\}
\end{equation}

That is to say, any number that can be represented by the division of two integers, where the denominator is non-zero.

Comparatively, a \textbf{real} number is any number that can be represented by an infinite decimal. For example, $\pi$ is real but not rational.

\subsubsection{Intervals}

Square brackets denote a \textbf{closed interval}:

\[
	[\,a, b\,] = \set{x \mid (a \leq x \leq b) \wedge (x \in \mathbb{R})}
\]

Round brackets denote a \textbf{open interval}:

\[
	(\,a, b\,) = \set{x \mid (a < x < b) \wedge (x \in \mathbb{R})}
\]

Note that square and round brackets can be mixed together, to have a closed interval on one side, and an open interval on the other.

\subsubsection{Russell's Paradox}

Naive set theory leads to Russell's paradox: \[S = \set{A \mid A \notin A}\]

This set leads to the paradoxical question: is $S$ a member of $S$?


\subsubsection{Set minus}

The symbol $\setminus$ refers to ``set minus'':

\[
	A \setminus B \equiv \set{a \mid (a \in A) \wedge (a \notin B)}
\]

That being, A minus B is equivalent to the set of all the items in A that are not in B.

\subsubsection{Powerset}

The symbol $\wp$ refers to the ``powerset'' of a set:

\[
	\wp(A) = \set{A' \mid A' \subseteq A}
\]

E.G. the powerset of the set $A = \set{x, y}$ is $\set{\set{}, \set{x}, \set{y}, \set{x, y}}$

\subsubsection{Cartesian Product}

The symbol $\times$ refers to the cartesian product of sets:

\[
	A \times B = \set{\left(a, b\right) \mid (a \in A) \wedge (b \in B)}
\]

This refers to the set of ordered pairs with first element from A and second from B.

\subsection{Functions}

A function $f:A \rightarrow B$ is an assignment of a unique element of B to every element of A.

For a function $f: A \rightarrow B$

\begin{itemize}
	\item $A$ is the \textbf{domain} of $f$
	\item $B$ is the \textbf{codomain} of $f$
	\item If $f(a) = b$ then $b$ is the \textbf{image} of $a$.
	\item The \textbf{pre-image} of $b$ is the set {a : f(a) = b}, or in other words, all of the values of $a$ that map to $b$.
	\item The \textbf{image} of $f$ is the set of images of elements of $A$. It is distinct to the codomain, in that the codomain is the conceivable set of possible results of the function, whereas the image is the actual set of the results of operating that function on a set.
\end{itemize}

\subsubsection{Composition}

If we have two functions $f:A \rightarrow B$ and $g: B \rightarrow C$ we can define the composition of $g$ and $f$ as $g \circ f$.

In other words:

\[
	g \circ f(x) = g(f(x))
\]

\subsubsection{Injections, Surjections, Bijections}

A function $f$ is either:

\begin{itemize}
	\item An \textbf{injection} (or 1-to-1) if for all $a,a' \in A$ if $f(a) = f(a')$ then $a = a'$
	\item A \textbf{surjection} (or onto) if each element of the codomain is mapped to by at least one element of the domain. Notationally: $\forall y \in Y, \exists x \in X \mid y = f(x) $
\end{itemize}

\section{Linear Algebra}

Abstractions do not just apply to programming - they are true of mathematics as well.

Axioms are in themselves abstractions - they define the rules through which mathematics operates.

\subsection{Groups}

A group is simply an abstract thing with some set $G$, a binary operation $\star$, and some axioms.

There are some rules:

\begin{itemize}
	\item $\star$ is \textbf{closed} on $G$: $G \times G \rightarrow G$
	\item $\star$ is \textbf{associative}, i.e. for all $a, b, c$ in $G$, $a \star (b \star c) = (a \star b) \star c$
	\item There exists an \textbf{identity} element: $e \in G$: $\forall a \in G.a \star e = e \star a = a$
	\item $\star$ has an \textbf{inverse operator} i.e. $\forall a \in G.\exists b \in G . a \star b = b \star a = e$
\end{itemize}

An example of a group would be the set of the real numbers $\mathbb{R}$, with the binary operation $+$.

Theorem: In a group, identity is unique.

Proof:

Suppose we have $e_1$ and $e_2$ which are both identities.

$e_1 \star e_2 = e_2$ because $e_1$ is the identity.

$e_1 \star e_2 = e_1$ because $e_2$ is the identity.

$ \therefore e_1 = e_2$.

\subsubsection{Finite Groups}

Modulo arithmetic can be used to study a finite group, e.g. $\mathbb{Z}_4$.
\begin{table}[ht]
	\centering
	\begin{tabular}{@{}l|lllll@{}}
		+ & 0 & 1 & 2 & 3 \\
		\hline
		0 & 0 & 1 & 2 & 3 \\
		1 & 1 & 2 & 3 & 0 \\
		2 & 2 & 3 & 0 & 1 \\
		3 & 3 & 0 & 1 & 2 \\
	\end{tabular}
\end{table}

\subsection{Fields}

A field is a set on which addition and multiplication are defined, binary operations. For a field to be valid, these operations must meet a set of conditions:

\begin{itemize}
	\item Associativity of addition and multiplication
	\item Commutativity of addition and multiplication
	\item Additive and multiplicative identity - these cannot be the same.
	\item Additive and multiplicative inverses for all elements
	\item Distributivity of multiplication over addition: $a \times (b + c) = (a \times b) + (a \times c)$
\end{itemize}

The set $\mathbb{Z}_n$ can form a field, if and only if $n$ is prime. This is because you do not get multiplicative inverses for all elements if $n$ is non-prime.

\subsection{Vector Space}

Vector space is a mathematical structure with two sets: scalars $F$ and vectors $V$.

$V$ forms a commutative group under addition: $u + v = v + u$. 

$F$ forms a field.

Vectors multiply with scalars to form vectors: $F \times V \rightarrow F$ 

\subsubsection{Notation Conventions}

Vector variables are written in \textbf{bold} i.e. $\mathbf{v, u}$

\subsubsection{Vector Space Axioms}

\begin{itemize}
  \item $a(b\mathbf{v} = (ab)\mathbf{v}$
  \item $1\mathbf{v} = \mathbf{v}$ 
  \item $a(\mathbf{u} + \mathbf{v}) = a\mathbf{u} + a\mathbf{v}$
  \item $(a+b)\mathbf{v}=a\mathbf{v} + b\mathbf{v}$
\end{itemize}

\subsubsection{Linear Combinations}

Given vectors $\mathbf{v}_1, \ldots, \mathbf{v}_n$ and scalars $a_1, \ldots, a_n$, we can form a \textbf{linear combination}:

\[
  \sum_{i=1}^{n} a_i \mathbf{v}_i
\]

\subsubsection{Span}

The set of all linear combinations of a set of vectors is called its \textbf{span}.

\subsubsection{Spanning set}

A set of vectors is a spanning set for a vector space $(F, V)$ if span($S$) = $V$

\subsubsection{Linear Dependence}

A set of vectors is linearly dependent if we can write one vector as a linear combination of the others.

An example of linearly independent vectors would be $\set{\ihat, \jhat, \khat}$. 

\subsubsection{Basis for a Vector Space}

A subset $S$ of vector space $V$ is a \textbf{basis} for $V$ if $S$ spans $V$
and $S$ is linearly independent.

The standard basis for $\mathbb{R}^3$ is $\set{\ihat, \jhat, \khat }$

\section{Automatic Differentiation}

Automatic differentiation is a simple way of differentiating exactly, using the
chain rule. 

If we break up a function into $e(f(g(h(x))))$ we can differentiate it into a
series of steps:

\begin{align*}
	\frac{dg}{dx} &= \frac{dg}{dh}\frac{dh}{dx} \\
	\frac{df}{dx} &= \frac{df}{dg}\frac{dg}{dx} \\
	\frac{de}{dx} &= \frac{de}{df}\frac{df}{dx} 
\end{align*} 

\subsection{Multivariate Chain Rule}

If we have $f(x,y):\mathbb{R}^2 \rightarrow \mathbb{R}$, where $x,y$ are
functions of $s, t$, we get:

$$
	\frac{df}{dt} = \frac{df}{dx} \frac{dx}{dt} + \frac{df}{dy} \frac{dy}{dt}
$$

\subsection{Computation Graphs}

You can write a "computation graph" from an expression. This splits up an
expression into a series of simpler functions, to allow you to easily calculate
a derivative.

