# The Gremlin Guide

The Gremlin Guide is a work-in-progress set of notes, written as I progress through my computer science degree at Durham university. Written in LaTeX. 

First year modules:
- COMP1021: Mathematics for Computer Science
- COMP1051: Computational Thinking
- COMP1071: Computer Systems
- COMP1081: Algorithms and Data Structures
- COMP1101: Programming (Black)
- MATH1031: Discrete Mathematics

Second year modules: TBD

Third year modules: TBD

## Download

To download, click [here](https://gitlab.com/jackaed/gremlin-guide/-/jobs/artifacts/master/download?job=build)

## License

Available under the GPL v2. I may relicense under a more permissive license in future, should it end up being in any way important. See the LICENSE file for details.
