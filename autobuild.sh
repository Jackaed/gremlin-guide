#!/usr/bin/env bash
cd "$(dirname "$0")"
find . -name "*.tex" | entr ./build.sh

